package repository;

import lombok.RequiredArgsConstructor;
import model.Student;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class StudentRepositoryImpl implements StudentRepository {
    private final RedisTemplate<String,Object> redisTemplate;
    public static final String HASH_KEY_NAME = "Student";
    @Override
    public Student saveStudent(Student student) {
        redisTemplate.opsForHash().put(HASH_KEY_NAME,student.getId(),student);
        return student;
    }

    @Override
    public Student updateStudent(Integer id, Student student) {
        Optional<Student> cachedStudent = Optional.ofNullable(findById(id));
        cachedStudent.orElseThrow(RuntimeException::new);
        redisTemplate.opsForHash().put(HASH_KEY_NAME,id,student);
        return student;
    }

    @Override
    public void deleteStudent(Integer id) {
        redisTemplate.opsForHash().delete(HASH_KEY_NAME,id);
    }

    @Override
    public Student findById(Integer id) {
        return (Student) redisTemplate.opsForHash().get(HASH_KEY_NAME,id);    }

    @Override
    public List<Object> findAll() {
        return redisTemplate.opsForHash().values(HASH_KEY_NAME);
    }
}

