package repository;

import model.Student;

import java.util.List;

public interface StudentRepository {
    Student saveStudent(Student student);
    Student updateStudent(Integer id, Student student);
    void deleteStudent(Integer id);
    Student findById(Integer id);
    List<Object> findAll();

}
